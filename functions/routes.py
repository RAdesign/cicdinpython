"""actual routing within main flask app happens here, defining internal paths for wrapper"""
from flask import Flask
from flask.wrappers import Response
from operations import get_channel  # actual interactions with the database using operations

# this is an internal flask application
flask_app = Flask("internal")


# gets channel id
@flask_app.route("/channel/<string:channel_id>", methods=["GET", "POST"])
def channel(channel_id: str):
    return get_channel(channel_id)


# simple response from channel API
@flask_app.route("/", methods=["GET"])
def index():
    return Response("Channel API is active.", status=200)
