"""main file containing entry points for flask application, and creating a context within,
all as aa idea of a program getting information from channels of a video streaming service"""
from flask.wrappers import Request, Response
from routes import flask_app


def channel_api(request: Request) -> Response:
    # make a new application context for internal flask_app
    app_context = flask_app.test_request_context(
        path=request.full_path,
        method=request.method,
    )
    app_context.request = request
    app_context.push()
    response = app_context.full_dispatch_request()
    app_context.pop()
    return response
