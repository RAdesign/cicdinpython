"""sample of creating pulumi environment, not working like this RN, can use Pulumi templates instead"""
import pulumi
from pulumi_gcp import cloudfunctions, storage

# path to where Cloud Functions source code is
PATH_TO_SOURCE_CODE = "./functions"

# storing source code to Cloud Functions in a Google Cloud Storage bucket
gc_bucket = storage.Bucket("channel_api_bucket", location="US", force_destroy=True)

# creating single Cloud Storage object, containing all the functions source code: main.py,requirements.txt
source_archive_object = storage.BucketObject(
    "channel_api",
    bucket=gc_bucket.name,
    source=pulumi.asset.FileArchive(PATH_TO_SOURCE_CODE),
)

# creating Cloud Function, deploying the source just uploaded to Google Cloud Storage
func_gc = cloudfunctions.Function(
    "channel_api",
    entry_point="channel_api",
    runtime="python310",
    source_archive_bucket=gc_bucket.name,
    source_archive_object=source_archive_object.name,
    trigger_http=True,
)

func_invoker = cloudfunctions.FunctionIamMember(
    "invoker",
    project=func_gc.project,
    region=func_gc.region,
    cloud_function=func_gc.name,
    role="roles/cloudfunctions.invoker",
    member="allUsers",
)

# exporting DNS name of the bucket and cloud function URL
pulumi.export("bucket_name", gc_bucket.url)
pulumi.export("func_url", func_gc.https_trigger_url)
