""" file for testing operations file and all params of database connection"""
from functools import partial

import pytest

from ..functions.operations import ChannelNotFoundError, get_channel

MOCK_DATABASE = "tests/mock_channels.db"
# supply path to mock/test db with partial
get_channel_mock = partial(get_channel, db_path=MOCK_DATABASE)


# a success test
def test_get_channel_success() -> None:
    channel = get_channel_mock("pythonalgos")
    assert channel["name"] == "Trading Algorithms in Python"


# a failed test
def test_get_channel_fail() -> None:
    with pytest.raises(ChannelNotFoundError):
        get_channel_mock("algotrading_2")
